# Quantum Chess

A chess variant inspired by quantum physics! The difference between ordinary chess and quantum chess is the ability to put your pieces in superpositions. Here’s how it works. 

<h1>Movement</h1>

There are five modes of movement you can choose between in quantum chess: Move, Split, Merge, Collapse, and Branchmove.

![](Media/Modes.png)

Modes 1 and 2 are for non-superposed pieces, and Modes 3, 4, and 5 are for non-superposed pieces.

<h2>Mode 1: Move</h2>

![](Media/ordinary-move.mov)

This mode allows you to move just as you would in an ordinary chess game: a non-superposed piece moving to a single position.

<h2>Mode 2: Split</h2>

In the split mode, you can choose a non-superposed piece and split it into a superposition of two squares.

![](Media/superposition.mov)

You can't choose a position that is occupied, even by a superposed piece, meaning that splitting moves can never be attacking moves.

![](Media/no-split-attack.mov)

One powerful strategy is to castle into a superposition, bringing out both rooks and forcing your opponent to gamble on which side of the board to stage an attack on.

![](Media/double-castle.mov)

<h2>Mode 3: Merge</h2>

In this mode you can merge the two branches of a superposed piece, recombining them onto a square that's accessible from both branches.

![](Media/merge.mov)

<h2>Mode 4: Collapse</h2>

Mode 4 is the riskiest mode. In this mode, you choose one of your superposed pieces and collapse it. There are two possible outcomes: first, it might collapse to the position you clicked on. In this case you now have a choice to either perform an ordinary move...

![](Media/collapse-take.mov)

... or to split it into a new superposition.

![](Media/collapse-split.mov)

But if you get unlucky, then your piece will collapse to the position you didn't select. In this case, your turn is over and it goes to your opponent.

![](Media/collapse-missed.mov)


<h2>Mode 5: Branch Move</h2>

Finally, in a branch move you relocate just one branch of the superposition, without collapsing the wavefunction or affecting the other branch.

![](Media/branch-move.mov)

Just like splitting moves, branch moves cannot be attacks.

<h1>Interactions</h1>

<h2>Attacking a Superposition</h2>

When you attack a superposed piece, the result is that the piece collapses. If it collapses onto the square you attacked, then the attack is successful and you take the piece.

![](Media/successful-attack.mov)

Otherwise, your piece moves harmlessly into the square you just attacked.

![](Media/failed-attack.mov)

This means that attacking a superposed piece is risky! It's possible for your attack to backfire, resulting in the attacker being captured next turn by the same piece it attacked.

![](Media/takeback.mov)

It's also possible for a pawn to move diagonally without taking a piece, in a failed attack.

![](Media/pawn-diagonal-move.mov)

<h2>Line of Sight</h2>

Superposed pieces block the lines of sight of both your pieces and your opponent's pieces. This allows you to defend your pieces or block open files, without fully committing to defensive positions.

![](Media/split-block.mov)

<h2>Winning the Game</h2>

To win quantum chess, you must actually take the opponent's king. Let's see why it's not enough to just get the opponent into a position that would ordinarily be checkmate:

![](Media/sequence-1.mov)

It's blue's turn now, and things look pretty lost. But look what blue can do:

![](Media/sequence-2.mov)

Now the red queen has to choose which of the two targets to attack, and there's a 50% chance that she gets it wrong!

![](Media/sequence-3b.mov)

So how can red get a guaranteed win? It takes a little patience. Rather than trying to attack one of the two squares, the red queen can hang back and wait for a turn.

![](Media/sequence-3c.mov)

Now the blue king can either leave superposition (losing immediately), or branch-move, giving the red queen a safe shot at the king. 

And that's quantum chess! Enjoy!

