import pygame
import Board
import Piece

pygame.init()
white,black,gray = (255,255,255),(0,0,0),(150,150,150)
red,blue,yellow,lightyellow = (255,0,0),(0,0,255),(255,255,0),(200,200,0)
fadedyellow1,fadedyellow2 = (254,255,191),(231,233,154)
fadedred,fadedblue,fadedgreen = (245,180,180),(180,180,245),(180,245,180)

s = 16 #scaling factor
screen = pygame.display.set_mode((50*s,50*s))
pygame.display.set_caption("Quantum Chess")

MOVE,SPLIT,MERGE,COLLAPSE,BRANCHMOVE = 0,1,2,3,4
mode1 = pygame.image.load("Images/Mode1.png")
mode2 = pygame.image.load("Images/Mode2.png")
mode3 = pygame.image.load("Images/Mode3.png")
mode4 = pygame.image.load("Images/Mode4.png")
mode5 = pygame.image.load("Images/Mode5.png")
mode1selected = pygame.image.load("Images/Mode1Selected.png")
mode2selected = pygame.image.load("Images/Mode2Selected.png")
mode3selected = pygame.image.load("Images/Mode3Selected.png")
mode4selected = pygame.image.load("Images/Mode4Selected.png")
mode5selected = pygame.image.load("Images/Mode5Selected.png")
scale = (4*s,4*s)
mode1 = pygame.transform.scale(mode1, scale)
mode2 = pygame.transform.scale(mode2, scale)
mode3 = pygame.transform.scale(mode3, scale)
mode4 = pygame.transform.scale(mode4, scale)
mode5 = pygame.transform.scale(mode5, scale)
mode1selected = pygame.transform.scale(mode1selected, scale)
mode2selected = pygame.transform.scale(mode2selected, scale)
mode3selected = pygame.transform.scale(mode3selected, scale)
mode4selected = pygame.transform.scale(mode4selected, scale)
mode5selected = pygame.transform.scale(mode5selected, scale)
mode_img = [mode1,mode2,mode3,mode4,mode5]
modeselected_img = [mode1selected,mode2selected,mode3selected,mode4selected,mode5selected]

red_pawn_img = pygame.image.load("Images/redpawn.png")
blue_pawn_img = pygame.image.load("Images/bluepawn.png")
red_knight_img = pygame.image.load("Images/redknight.png")
blue_knight_img = pygame.image.load("Images/blueknight.png")
red_bishop_img = pygame.image.load("Images/redbishop.png")
blue_bishop_img = pygame.image.load("Images/bluebishop.png")
red_rook_img = pygame.image.load("Images/redrook.png")
blue_rook_img = pygame.image.load("Images/bluerook.png")
red_queen_img = pygame.image.load("Images/redqueen.png")
blue_queen_img = pygame.image.load("Images/bluequeen.png")
red_king_img = pygame.image.load("Images/redking.png")
blue_king_img = pygame.image.load("Images/blueking.png")
faded_red_pawn_img = pygame.image.load("Images/fadedredpawn.png")
faded_blue_pawn_img = pygame.image.load("Images/fadedbluepawn.png")
faded_red_knight_img = pygame.image.load("Images/fadedredknight.png")
faded_blue_knight_img = pygame.image.load("Images/fadedblueknight.png")
faded_red_bishop_img = pygame.image.load("Images/fadedredbishop.png")
faded_blue_bishop_img = pygame.image.load("Images/fadedbluebishop.png")
faded_red_rook_img = pygame.image.load("Images/fadedredrook.png")
faded_blue_rook_img = pygame.image.load("Images/fadedbluerook.png")
faded_red_queen_img = pygame.image.load("Images/fadedredqueen.png")
faded_blue_queen_img = pygame.image.load("Images/fadedbluequeen.png")
faded_red_king_img = pygame.image.load("Images/fadedredking.png")
faded_blue_king_img = pygame.image.load("Images/fadedblueking.png")
scale = (3*s,8*s)
red_pawn_img = pygame.transform.scale(red_pawn_img, scale)
blue_pawn_img = pygame.transform.scale(blue_pawn_img, scale)
red_knight_img = pygame.transform.scale(red_knight_img, scale)
blue_knight_img = pygame.transform.scale(blue_knight_img, scale)
red_bishop_img = pygame.transform.scale(red_bishop_img, scale)
blue_bishop_img = pygame.transform.scale(blue_bishop_img, scale)
red_rook_img = pygame.transform.scale(red_rook_img, scale)
blue_rook_img = pygame.transform.scale(blue_rook_img, scale)
red_queen_img = pygame.transform.scale(red_queen_img, scale)
blue_queen_img = pygame.transform.scale(blue_queen_img, scale)
red_king_img = pygame.transform.scale(red_king_img, scale)
blue_king_img = pygame.transform.scale(blue_king_img, scale)
faded_red_pawn_img = pygame.transform.scale(faded_red_pawn_img, scale)
faded_blue_pawn_img = pygame.transform.scale(faded_blue_pawn_img, scale)
faded_red_knight_img = pygame.transform.scale(faded_red_knight_img, scale)
faded_blue_knight_img = pygame.transform.scale(faded_blue_knight_img, scale)
faded_red_bishop_img = pygame.transform.scale(faded_red_bishop_img, scale)
faded_blue_bishop_img = pygame.transform.scale(faded_blue_bishop_img, scale)
faded_red_rook_img = pygame.transform.scale(faded_red_rook_img, scale)
faded_blue_rook_img = pygame.transform.scale(faded_blue_rook_img, scale)
faded_red_queen_img = pygame.transform.scale(faded_red_queen_img, scale)
faded_blue_queen_img = pygame.transform.scale(faded_blue_queen_img, scale)
faded_red_king_img = pygame.transform.scale(faded_red_king_img, scale)
faded_blue_king_img = pygame.transform.scale(faded_blue_king_img, scale)

PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING = 1,2,3,4,5,6
RED, BLUE = 1, -1
images = {RED*PAWN:red_pawn_img,
          BLUE*PAWN:blue_pawn_img,
          RED*KNIGHT:red_knight_img,
          BLUE*KNIGHT:blue_knight_img,
          RED*BISHOP:red_bishop_img,
          BLUE*BISHOP:blue_bishop_img,
          RED*ROOK:red_rook_img,
          BLUE*ROOK:blue_rook_img,
          RED*QUEEN:red_queen_img,
          BLUE*QUEEN:blue_queen_img,
          RED*KING:red_king_img,
          BLUE*KING:blue_king_img}
faded_images = {RED*PAWN:faded_red_pawn_img,
          BLUE*PAWN:faded_blue_pawn_img,
          RED*KNIGHT:faded_red_knight_img,
          BLUE*KNIGHT:faded_blue_knight_img,
          RED*BISHOP:faded_red_bishop_img,
          BLUE*BISHOP:faded_blue_bishop_img,
          RED*ROOK:faded_red_rook_img,
          BLUE*ROOK:faded_blue_rook_img,
          RED*QUEEN:faded_red_queen_img,
          BLUE*QUEEN:faded_blue_queen_img,
          RED*KING:faded_red_king_img,
          BLUE*KING:faded_blue_king_img}

def coordToPos(n,m):
    return 6*s+n*5*s, 4*s+m*5*s

def posToCoord(x,y):
    for i in range(8):
        for j in range(8):
            if 5*s + i*5*s < x < 10*s + i*5*s and 5*s + j*5*s < y < 10*s + j*5*s:
                return i,j
    return None

def modeButton(x,y):
    if 10 < y < 10+4*s:
        if 0 < x-10 < 4*s:
            return 0
        if 5*s < x-10 < 9*s:
            return 1
        if 10*s < x-10 < 14*s:
            return 2
        if 15*s < x-10 < 19*s:
            return 3
        if 20*s < x-10 < 24*s:
            return 4
    return None

def printGameState(board):
    s = "\npieces = ["
    for piece in board.pieces:
        s += "("
        s += str(piece.team) + ","
        s += str(piece.pieceType) + ","
        positions = piece.getPositions()
        if len(positions) == 1:
            s += str(positions[0][0]) + ","
            s += str(positions[0][1]) + "), "
        elif len(positions) == 2:
            s += str(positions[0][0]) + ","
            s += str(positions[0][1]) + ","
            s += str(positions[1][0]) + ","
            s += str(positions[1][1]) + "), "
    s = s[:-2]
    s += "]"
    s += "\ncanCastleLeft=" + str(board.canCastleLeft)
    s += "\ncanCastleRight=" + str(board.canCastleRight)
    print(s)

selected = None
hovering = None
stuck = False
toMove = []
turn = 0
mode = MOVE

board = Board.Board(screen)

'''
Here is where you would enter the info to load a gamestate. Example:

pieces = [Piece.Piece(-1,1,0,6), Piece.Piece(-1,1,1,6), Piece.Piece(-1,1,2,6), Piece.Piece(-1,1,3,6), Piece.Piece(-1,1,4,4), Piece.Piece(-1,1,5,6), Piece.Piece(-1,1,6,6), Piece.Piece(-1,1,7,6), Piece.Piece(1,1,0,1), Piece.Piece(1,1,1,1), Piece.Piece(1,1,2,1), Piece.Piece(1,1,3,2), Piece.Piece(1,1,4,1), Piece.Piece(1,1,5,1), Piece.Piece(1,1,6,1), Piece.Piece(1,1,7,1), Piece.Piece(-1,4,0,7), Piece.Piece(-1,4,7,7), Piece.Piece(-1,2,1,7), Piece.Piece(-1,2,5,5,4,6), Piece.Piece(-1,3,2,7), Piece.Piece(-1,3,5,7), Piece.Piece(-1,5,3,7), Piece.Piece(-1,6,4,7), Piece.Piece(1,4,0,0), Piece.Piece(1,4,7,0), Piece.Piece(1,2,1,0), Piece.Piece(1,2,6,0), Piece.Piece(1,3,3,1,6,4), Piece.Piece(1,3,5,0), Piece.Piece(1,5,3,0), Piece.Piece(1,6,4,0)]
canCastleLeft={-1: True, 1: True}
canCastleRight={-1: True, 1: True}

board.loadGame(self,pieces,canCastleLeft,canCastleRight)
'''

running = True
while running:
    screen.fill(black)
    mouseX, mouseY = pygame.mouse.get_pos()
    
    for i in range(8):
        for j in range(8):
            color = white if (i+j)%2 else gray
            pygame.draw.rect(screen,color,[5*s+5*i*s,5*s+5*j*s,5*s,5*s])

    for i in range(5):
        screen.blit(mode_img[i],(10+5*i*s,10))
    screen.blit(modeselected_img[mode],(10+5*mode*s,10))

    if selected is not None:
        positions = selected.getPositions()
        if mode == MERGE:
            for pos in positions:
                pygame.draw.rect(screen,yellow,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
            if board.validMerges(selected) is not None:
                for pos in board.validMerges(selected):
                    pygame.draw.rect(screen,lightyellow,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
        elif mode == MOVE or mode == SPLIT:
            for pos in positions:
                pygame.draw.rect(screen,yellow,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
            if board.validMoves(selected,mode) is not None:
                for pos in board.validMoves(selected,mode):
                    pygame.draw.rect(screen,lightyellow,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
            if len(toMove) == 1:
                pygame.draw.rect(screen,yellow,[5*s+5*s*toMove[0][0],5*s+5*s*toMove[0][1],5*s,5*s])
        elif mode == BRANCHMOVE:
            for pos in positions:
                pygame.draw.rect(screen,yellow,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
            if board.validMovesSingleBranch(selected,mode,whichBranch) is not None:
                for pos in board.validMovesSingleBranch(selected,mode,whichBranch):
                    pygame.draw.rect(screen,lightyellow,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])

    if hovering is not None:
        positions = hovering.getPositions()
        if hovering.team != (turn%2)*2 - 1:
            for pos in positions:
                pygame.draw.rect(screen,fadedgreen,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
        elif mode == MERGE:
            for pos in positions:
                pygame.draw.rect(screen,fadedgreen,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
            if board.validMerges(hovering) is not None:
                for pos in board.validMerges(hovering):
                    pygame.draw.rect(screen,fadedyellow1,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
        elif mode == MOVE or mode == SPLIT:
            for pos in positions:
                pygame.draw.rect(screen,fadedyellow2,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
            if board.validMoves(hovering,mode) is not None:
                for pos in board.validMoves(hovering,mode):
                    pygame.draw.rect(screen,fadedyellow1,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
        else:
            if hovering.isSuperposed():
                which = None
                if posToCoord(mouseX,mouseY) is not None:
                    n,m = posToCoord(mouseX,mouseY)
                    if (n,m) == positions[0]:
                        which = 0
                    elif (n,m) == positions[1]:
                        which = 1
                if which is not None:
                    for pos in positions:
                        pygame.draw.rect(screen,fadedgreen,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])
                    if board.validMovesSingleBranch(hovering,mode,which) is not None:
                        for pos in board.validMovesSingleBranch(hovering,mode,which):
                            pygame.draw.rect(screen,fadedyellow1,[5*s+5*s*pos[0],5*s+5*s*pos[1],5*s,5*s])

    for piece in board.pieces:
        pieceType = piece.pieceType
        team = piece.team
        positions = piece.getPositions()
        if piece.isSuperposed():
            n0,m0 = positions[0]
            n1,m1 = positions[1]
            x0,y0 = coordToPos(n0,m0)
            x1,y1 = coordToPos(n1,m1)
            screen.blit(faded_images[team*pieceType], (x0,y0))
            screen.blit(faded_images[team*pieceType], (x1,y1))
        else:
            n,m = positions[0]
            x,y = coordToPos(n,m)
            screen.blit(images[team*pieceType], (x,y))

    for event in pygame.event.get():
        if selected is None:
            if posToCoord(mouseX,mouseY) is not None:
                n,m = posToCoord(mouseX,mouseY)
                piece = board.pieceAt(n,m)
                if piece is not None:
                    if piece.team == (turn%2)*2 - 1:
                        if (mode == MOVE or mode == SPLIT) and not piece.isSuperposed():
                            hovering = piece
                        elif (mode == MERGE or mode == COLLAPSE or mode == BRANCHMOVE) and piece.isSuperposed():
                            hovering = piece
                    elif piece.isSuperposed():
                        hovering = piece
                else:
                    hovering = None
            else:
                hovering = None
        
        if event.type == pygame.MOUSEBUTTONUP:
            if modeButton(mouseX,mouseY) is not None:
                if not stuck:
                    mode = modeButton(mouseX,mouseY)
                    selected = None
                    toMove = []
                else:
                    if modeButton(mouseX,mouseY)==MOVE or modeButton(mouseX,mouseY)==SPLIT:
                        mode = modeButton(mouseX,mouseY)
                        toMove = []
            
            if posToCoord(mouseX,mouseY) is not None:
                n,m = posToCoord(mouseX,mouseY)
                piece = board.pieceAt(n,m)
                if piece is not None and not stuck:
                    if piece.team == (turn%2)*2 - 1:
                        if (mode == MOVE or mode == SPLIT) and not piece.isSuperposed():
                            selected = piece
                            hovering = None
                        elif mode == MERGE and piece.isSuperposed():
                            selected = piece
                            hovering = None
                            if (n,m) == piece.getPositions()[0]:
                                whichBranch = 0
                            if (n,m) == piece.getPositions()[1]:
                                whichBranch = 1
                        elif mode == COLLAPSE and piece.isSuperposed():
                            board.collapse(piece)
                            hovering = None
                            if (n,m) in piece.getPositions():
                                mode = MOVE
                                stuck = True
                                selected = piece
                            else:
                                selected = None
                                turn += 1
                                printGameState(board)
                                mode = MOVE
                        elif mode == BRANCHMOVE and piece.isSuperposed():
                            selected = piece
                            hovering = None
                            if (n,m) == piece.getPositions()[0]:
                                whichBranch = 0
                            if (n,m) == piece.getPositions()[1]:
                                whichBranch = 1
                            
                if selected is not None:
                    if mode == MERGE:
                        if board.validMerges(selected) is not None:
                            if (n,m) in board.validMerges(selected):
                                if board.pieceAt(n,m) is not None:
                                    if board.pieceAt(n,m).isSuperposed():
                                        board.collapse(board.pieceAt(n,m))
                                if board.pieceAt(n,m) is not None:
                                    board.pieces.remove(board.pieceAt(n,m))
                                if selected.pieceType == KING and abs(selected.getPositions()[0][0] - n) > 1:
                                    if n == 2:
                                        board.move(board.pieceAt(0,m),3,m)
                                    if n == 6:
                                        board.move(board.pieceAt(7,m),5,m)
                                board.move(selected,n,m)
                                turn += 1
                                printGameState(board)
                                mode = MOVE
                                selected = None
                                toMove = []
                            elif piece == None:
                                selected = None
                                toMove = []
                            elif not piece.isSuperposed():
                                selected = None
                                toMove = []
                    elif mode == MOVE or mode == SPLIT:
                        if (n,m) in board.validMoves(selected,mode):
                            if mode == MOVE:
                                if board.pieceAt(n,m) is not None:
                                    if board.pieceAt(n,m).isSuperposed():
                                        board.collapse(board.pieceAt(n,m))
                                if board.pieceAt(n,m) is not None:
                                    board.pieces.remove(board.pieceAt(n,m))
                                if selected.pieceType == KING and abs(selected.getPositions()[0][0] - n) > 1:
                                    if n == 2:
                                        board.move(board.pieceAt(0,m),3,m)
                                    if n == 6:
                                        board.move(board.pieceAt(7,m),5,m)
                                board.move(selected,n,m)
                                if selected.pieceType == PAWN:
                                    if selected.team == BLUE and m == 0:
                                        selected.promote()
                                    if selected.team == RED and m == 7:
                                        selected.promote()
                                turn += 1
                                printGameState(board)
                                mode = MOVE
                                selected = None
                                toMove = []
                                stuck = False
                            elif mode == SPLIT:
                                if len(toMove) < 2:
                                    if (n,m) not in toMove:
                                        toMove.append((n,m))
                                    else:
                                        toMove = []
                                if len(toMove) == 2:
                                    x1,y1 = toMove[0]
                                    x2,y2 = toMove[1]
                                    if selected.pieceType == KING and abs(selected.getPositions()[0][0] - n) > 1:
                                        board.move(board.pieceAt(0,m),3,m)
                                        board.move(board.pieceAt(7,m),5,m)
                                    board.split(selected,x1,y1,x2,y2)
                                    turn += 1
                                    printGameState(board)
                                    mode = MOVE
                                    selected = None
                                    toMove = []
                                    stuck = False
                        elif piece == None and not stuck:
                            selected = None
                            toMove = []
                    elif mode == BRANCHMOVE:
                        if board.validMovesSingleBranch(selected,mode,whichBranch) is not None:
                            if (n,m) in board.validMovesSingleBranch(selected,mode,whichBranch):
                                board.branchmove(selected,whichBranch,n,m)
                                turn += 1
                                printGameState(board)
                                mode = MOVE
                                selected = None
                                toMove = []
                            elif piece == None:
                                selected = None
                                toMove = []
                            elif not piece.isSuperposed():
                                selected = None
                                toMove = []
                        
        
        if event.type == pygame.QUIT:
            running = False
            pygame.display.quit()

    pygame.display.flip()
