QUEEN = 5

class Piece:

    def __init__(self, team, pieceType, x1, y1, x2 = None, y2 = None):
        self.team = team
        self.pieceType = pieceType
        if x2 is None:
            self.positions = [(x1,y1)]
        else:
            self.positions = [(x1,y1),(x2,y2)]

    def isSuperposed(self):
        return len(self.positions) > 1

    def getPositions(self):
        return self.positions

    def promote(self):
        self.pieceType = QUEEN
