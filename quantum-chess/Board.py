
import Piece
import random

MOVE,SPLIT,MERGE,COLLAPSE,BRANCHMOVE = 0,1,2,3,4
PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING = 1,2,3,4,5,6
BLUE,RED = -1,1
white,black,gray = (255,255,255),(0,0,0),(150,150,150)

class Board:
    canCastleLeft = {BLUE:True, RED:True}
    canCastleRight = {BLUE:True, RED:True}

    bluepawns = [Piece.Piece(BLUE,PAWN,i,6) for i in range(8)]
    redpawns = [Piece.Piece(RED,PAWN,i,1) for i in range(8)]
    otherblues = [Piece.Piece(BLUE,ROOK,0,7),Piece.Piece(BLUE,ROOK,7,7),
                  Piece.Piece(BLUE,KNIGHT,1,7),Piece.Piece(BLUE,KNIGHT,6,7),
                  Piece.Piece(BLUE,BISHOP,2,7),Piece.Piece(BLUE,BISHOP,5,7),
                  Piece.Piece(BLUE,QUEEN,3,7),Piece.Piece(BLUE,KING,4,7)]
    otherreds = [Piece.Piece(RED,ROOK,0,0),Piece.Piece(RED,ROOK,7,0),
                  Piece.Piece(RED,KNIGHT,1,0),Piece.Piece(RED,KNIGHT,6,0),
                  Piece.Piece(RED,BISHOP,2,0),Piece.Piece(RED,BISHOP,5,0),
                  Piece.Piece(RED,QUEEN,3,0),Piece.Piece(RED,KING,4,0)]
    pieces = bluepawns + redpawns + otherblues + otherreds
    
    def __init__(self, screen):
        self.screen = screen

    def loadGame(self, pieces, canCastleLeft, canCastleRight):
        self.pieces = []
        for piece in pieces:
            if len(piece) == 4:
                self.pieces.append(Piece.Piece(piece[0],piece[1],piece[2],piece[3]))
            else:
                self.pieces.append(Piece.Piece(piece[0],piece[1],piece[2],piece[3],piece[4],piece[5]))
        self.canCastleLeft = canCastleLeft
        self.canCastleRight = canCastleRight

    def pieceAt(self,n,m):
        for piece in self.pieces:
            if (n,m) in piece.getPositions():
                return piece
        return None

    def isUnderAttack(self,n,m,team):
        for piece in self.pieces:
            if piece.team == team and piece.pieceType != KING:
                if (n,m) in self.validMoves(piece,MOVE):
                    return True
                if piece.isSuperposed():
                    if (n,m) in self.validMerges(piece):
                        return True
                    for i in range(2):
                        if (n,m) in self.validMovesSingleBranch(piece,COLLAPSE,i):
                            return True
                        if (n,m) in self.validMovesSingleBranch(piece,BRANCHMOVE,i):
                            return True
        return False

    def move(self,piece,n,m):
        if piece in self.pieces:
            piece.positions = [(n,m)]
        if piece.pieceType == KING:
            self.canCastleLeft[piece.team] = False
            self.canCastleRight[piece.team] = False
        if piece.pieceType == ROOK:
            if piece.getPositions()[0][0] == 0:
                self.canCastleLeft[piece.team] = False
            if piece.getPositions()[0][0] == 7:
                self.canCastleRight[piece.team] = False

    def split(self,piece,n,m,j,k):
        if piece in self.pieces:
            piece.positions = [(n,m),(j,k)]
        if piece.pieceType == KING:
            self.canCastleLeft[piece.team] = False
            self.canCastleRight[piece.team] = False

    def collapse(self,piece):
        if piece in self.pieces:
            if random.random() < .5:
                piece.positions = [piece.positions[0]]
            else:
                piece.positions = [piece.positions[1]]

    def branchmove(self,piece,whichBranch,n,m):
        if piece in self.pieces:
            piece.positions[whichBranch] = (n,m)

    def validMerges(self,piece):            
        valid = []
        valid1,valid2 = [],[]
        team = piece.team
        pieceType = piece.pieceType
        positions = piece.getPositions()
        x,y = positions[0]
        if pieceType == PAWN:
            valid1 = valid1 + self.validPawnMoves(team,x,y,True)
        elif pieceType == ROOK:
            valid1 = valid1 + self.validRookMoves(team,x,y,True)
        elif piece.pieceType == BISHOP:
            valid1 = valid1 + self.validBishopMoves(team,x,y,True)
        elif piece.pieceType == QUEEN:
            valid1 = valid1 + self.validQueenMoves(team,x,y,True)
        elif piece.pieceType == KING:
            valid1 = valid1 + self.validKingMoves(team,x,y,True,MERGE)
        elif piece.pieceType == KNIGHT:
            valid1 = valid1 + self.validKnightMoves(team,x,y,True)

        if not piece.isSuperposed():
            return valid
        
        x,y = positions[1]
        if pieceType == PAWN:
            valid2 = valid2 + self.validPawnMoves(team,x,y,True)
        elif pieceType == ROOK:
            valid2 = valid2 + self.validRookMoves(team,x,y,True)
        elif piece.pieceType == BISHOP:
            valid2 = valid2 + self.validBishopMoves(team,x,y,True)
        elif piece.pieceType == QUEEN:
            valid2 = valid2 + self.validQueenMoves(team,x,y,True)
        elif piece.pieceType == KING:
            valid2 = valid2 + self.validKingMoves(team,x,y,True,MERGE)
        elif piece.pieceType == KNIGHT:
            valid2 = valid2 + self.validKnightMoves(team,x,y,True)

        for pos in valid1:
            if pos in valid2:
                valid.append(pos)
        return valid

    def validMoves(self,piece,mode):
        valid = []
        team = piece.team
        pieceType = piece.pieceType
        positions = piece.getPositions()
        x,y = positions[0]

        canTake = (mode == MOVE or mode == MERGE)

        if pieceType == PAWN:
            valid = valid + self.validPawnMoves(team,x,y,canTake)
        elif pieceType == ROOK:
            valid = valid + self.validRookMoves(team,x,y,canTake)
        elif piece.pieceType == BISHOP:
            valid = valid + self.validBishopMoves(team,x,y,canTake)
        elif piece.pieceType == QUEEN:
            valid = valid + self.validQueenMoves(team,x,y,canTake)
        elif piece.pieceType == KING:
            valid = valid + self.validKingMoves(team,x,y,canTake,mode)
        elif piece.pieceType == KNIGHT:
            valid = valid + self.validKnightMoves(team,x,y,canTake)
        return valid

    def validMovesSingleBranch(self,piece,mode,which):
        valid = []
        team = piece.team
        pieceType = piece.pieceType
        positions = piece.getPositions()
        x,y = positions[which]

        canTake = (mode == COLLAPSE)

        if pieceType == PAWN:
            valid = valid + self.validPawnMoves(team,x,y,canTake)
        elif pieceType == ROOK:
            valid = valid + self.validRookMoves(team,x,y,canTake)
        elif piece.pieceType == BISHOP:
            valid = valid + self.validBishopMoves(team,x,y,canTake)
        elif piece.pieceType == QUEEN:
            valid = valid + self.validQueenMoves(team,x,y,canTake)
        elif piece.pieceType == KING:
            valid = valid + self.validKingMoves(team,x,y,canTake,mode)
        elif piece.pieceType == KNIGHT:
            valid = valid + self.validKnightMoves(team,x,y,canTake)

        return valid
    
    def validPawnMoves(self,team,x,y,canTake):
        valid = []
        if team == BLUE:
            if y == 6 and self.pieceAt(x,y-2) is None:
                if self.pieceAt(x,y-1) is None:
                    valid.append((x,y-2))               
        if team == RED:
            if y == 1 and self.pieceAt(x,y+2) is None:
                if self.pieceAt(x,y+1) is None:
                    valid.append((x,y+2))
        if self.pieceAt(x,y+team) is None:
            valid.append((x,y+team))
        if canTake:
            if self.pieceAt(x-1,y+team) is not None:
                if self.pieceAt(x-1,y+team).team != team:
                    valid.append((x-1,y+team))
            if self.pieceAt(x+1,y+team) is not None:
                if self.pieceAt(x+1,y+team).team != team:
                    valid.append((x+1,y+team))
        return valid

    def validRookMoves(self,team,x,y,canTake):
        valid = []
        for i in [-1,1]:
            lineOfSight = True
            k = 1
            while lineOfSight:
                if 0 <= x+i*k < 8:
                    if self.pieceAt(x+i*k,y) is None:
                        valid.append((x+i*k,y))
                    elif self.pieceAt(x+i*k,y).team != team:
                        if canTake:
                            valid.append((x+i*k,y))
                        lineOfSight = False
                    elif self.pieceAt(x,y) == self.pieceAt(x+i*k,y):
                        lineOfSight = True
                    else:
                        lineOfSight = False
                else:
                    lineOfSight = False
                k += 1
        for i in [-1,1]:
            lineOfSight = True
            k = 1
            while lineOfSight:
                if 0 <= y+i*k < 8:
                    if self.pieceAt(x,y+i*k) is None:
                        valid.append((x,y+i*k))
                    elif self.pieceAt(x,y+i*k).team != team:
                        if canTake:
                            valid.append((x,y+i*k))
                        lineOfSight = False
                    elif self.pieceAt(x,y) == self.pieceAt(x,y+i*k):
                        lineOfSight = True
                    else:
                        lineOfSight = False
                else:
                    lineOfSight = False
                k += 1
        return valid

    def validKnightMoves(self,team,x,y,canTake):
        valid = []
        for i in [-1,1]:
            for j in [-1,1]:
                if 0 <= x+2*i < 8 and 0 <= y+j < 8:
                    if self.pieceAt(x+2*i,y+j) is None:
                        valid.append((x+2*i,y+j))
                    elif self.pieceAt(x+2*i,y+j).team != team:
                        if canTake:
                            valid.append((x+2*i,y+j))
                if 0 <= x+i < 8 and 0 <= y+2*j < 8:
                    if self.pieceAt(x+i,y+2*j) is None:
                        valid.append((x+i,y+2*j))
                    elif self.pieceAt(x+i,y+2*j).team != team:
                        if canTake:
                            valid.append((x+i,y+2*j))
        return valid

    def validBishopMoves(self,team,x,y,canTake):
        valid = []
        for i in [-1,1]:
            for j in [-1,1]:
                lineOfSight = True
                k = 1
                while lineOfSight:
                    if 0 <= y+j*k < 8 and 0 <= x+i*k < 8:
                        if self.pieceAt(x+i*k,y+j*k) is None:
                            valid.append((x+i*k,y+j*k))
                        elif self.pieceAt(x+i*k,y+j*k).team != team:
                            if canTake:
                                valid.append((x+i*k,y+j*k))
                            lineOfSight = False
                        elif self.pieceAt(x,y) == self.pieceAt(x+i*k,y+j*k):
                            lineOfSight = True
                        else:
                            lineOfSight = False
                    else:
                        lineOfSight = False
                    k += 1
        return valid

    def validQueenMoves(self,team,x,y,canTake):
        valid = []
        for i in [-1,0,1]:
            for j in [-1,0,1]:
                if i != 0 or j != 0:
                    lineOfSight = True
                    k = 1
                    while lineOfSight:
                        if 0 <= y+j*k < 8 and 0 <= x+i*k < 8:
                            if self.pieceAt(x+i*k,y+j*k) is None:
                                valid.append((x+i*k,y+j*k))
                            elif self.pieceAt(x+i*k,y+j*k).team != team:
                                if canTake:
                                    valid.append((x+i*k,y+j*k))
                                lineOfSight = False
                            elif self.pieceAt(x,y) != self.pieceAt(x+i*k,y+j*k):
                                lineOfSight = False
                        else:
                            lineOfSight = False
                        k += 1
        return valid

    def validKingMoves(self,team,x,y,canTake,mode):
        valid = []
        for i in [-1,0,1]:
            for j in [-1,0,1]:
                if i != 0 or j != 0:
                    if 0 <= y+j < 8 and 0 <= x+i < 8:
                        if self.pieceAt(x+i,y+j) is None:
                            valid.append((x+i,y+j))
                        elif self.pieceAt(x+i,y+j).team != team:
                            if canTake:
                                valid.append((x+i,y+j))
        if self.canCastleRight[team]:
            if self.pieceAt(x+1,y) is None and self.pieceAt(x+2,y) is None:
                if not self.isUnderAttack(x+1,y,-team):
                    if not self.isUnderAttack(x+2,y,-team):
                        valid.append((x+2,y))
        if self.canCastleLeft[team]:
            if self.pieceAt(x-1,y) is None and self.pieceAt(x-2,y) is None and self.pieceAt(x-3,y) is None:
                if not self.isUnderAttack(x-1,y,-team):
                    if not self.isUnderAttack(x-2,y,-team):
                        if not self.isUnderAttack(x-3,y,-team):
                            valid.append((x-2,y))
        return valid

